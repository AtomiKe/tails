# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-23 16:49+0000\n"
"PO-Revision-Date: 2015-10-09 17:14+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/"
"first_steps_persistence_use/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Enable & use the persistent volume\"]]\n"
msgid "[[!meta title=\"Unlocking and using the Persistent Storage\"]]\n"
msgstr "[[!meta title=\"فعال کردن و استفاده از درایو مانا\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/persistence.caution.fa\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "Enable the persistent volume\n"
msgid "Unlocking the Persistent Storage"
msgstr "فعال کردن درایو مانا\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "1. When starting Tails, in the\n"
#| "<span class=\"guilabel\">Use persistence?</span> dialog of [[Tails\n"
#| "Greeter|welcome_screen]], choose <span class=\"guilabel\">Yes</span> to\n"
#| "enable the persistent volume for the current working session.\n"
msgid ""
"When starting Tails, in the\n"
"<span class=\"guilabel\">Encrypted Persistent Storage</span> section of\n"
"the [[Welcome Screen|welcome_screen]], enter your passphrase and click\n"
"<span class=\"button\">Unlock</span>.\n"
msgstr ""
"۱. وقتی تیلز را راه‌اندازی می‌کنید، در \n"
"پنجرهٔ <span class=\"guilabel\">استفاده از مانا؟</span> در [[خوشامدگوی تیلز|welcome_screen]] <span class=\"guilabel\">بله</span> را انتخاب کنید تا\n"
"درایو مانا را برای نشست کاری فعلی فعال شود.\n"

#. type: Plain text
#, no-wrap
msgid "[[!img welcome_screen/persistence.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img welcome_screen/persistence.png link=\"no\" alt=\"\"]]\n"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "Use the persistent volume\n"
msgid "Using the Persistent Storage"
msgstr "استفاده از درایو مانا\n"

#. type: Plain text
msgid ""
"After you unlock the Persistent Storage, the data corresponding to each "
"feature of the Persistent Storage is automatically available. For example:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Your personal files in the *Persistent* folder are accessible from "
"**Places**&nbsp;▸ **Persistent**."
msgstr ""

#. type: Plain text
msgid ""
"- Emails are available in *Thunderbird* and bookmarks are available in *Tor "
"Browser*."
msgstr ""

#. type: Plain text
msgid "- Additional software is automatically installed when starting Tails."
msgstr ""

#. type: Plain text
#, fuzzy
msgid ""
"For advanced users to access the internal content of the Persistent Storage, "
"choose **Places**&nbsp;▸ **Computer** and open the folders *live*&nbsp;▸ "
"*persistence*&nbsp;▸ *TailsData_unlocked*."
msgstr ""
"برای کاربران پیشرفته‌ای که می‌خواهند کنترل درایو مانا را در دست بگیرند،\n"
"مسیر زیر را بروید:\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Places</span>&nbsp;◀\n"
"  <span class=\"guimenuitem\">رایانه</span></span>, and open the folders\n"
"  <span class=\"filename\">لایو</span>&nbsp;◀\n"
"  <span class=\"filename\">مانا</span>&nbsp;◀\n"
"  <span class=\"filename\">TailsData_unlocked</span>.\n"

#~ msgid ""
#~ "To open the <span class=\"filename\">Persistent</span> folder and access "
#~ "your\n"
#~ "personal files and working documents, choose \n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Places</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Persistent</span></span>.\n"
#~ msgstr ""
#~ "برای باز کردن پوشهٔ <span class=\"filename\">مانا</span> و دسترسی به\n"
#~ "فایل‌های شخصی و سندهای کاری، مسیر زیر را بروید:\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Places</span>&nbsp;◀\n"
#~ "  <span class=\"guimenuitem\">مانا</span></span>\n"

#, fuzzy
#~| msgid ""
#~| "2. Enter the passphrase of the persistent volume in the\n"
#~| "<span class=\"guilabel\">Passphrase</span> text box.</span>\n"
#~ msgid ""
#~ "2. Enter the passphrase of the persistent volume in the\n"
#~ "<span class=\"guilabel\">Passphrase</span> text box.\n"
#~ msgstr ""
#~ "۲. گذرواژهٔ درایو مانا را در\n"
#~ "در بخش <span class=\"guilabel\">گذرواژه</span> وارد کنید.\n"

#~ msgid ""
#~ "3. If you select the <span class=\"guilabel\">Read-Only</span> check box, "
#~ "the\n"
#~ "content of persistent volume will be available and you will be able to "
#~ "modify\n"
#~ "it but the changes will not be saved.\n"
#~ msgstr ""
#~ "۳. اگر گزینهٔ <span class=\"guilabel\">فقط خواندن</span> را انتخاب "
#~ "کرده‌اید،\n"
#~ "محتویات درایو مانا قابل دسترسی خواهد بود و می‌توانید آن‌ها را تغییر دهید\n"
#~ "اما تغییرات ذخیره نخواهند شد.\n"
